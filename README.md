# Smithay's Wayland Source

This crate contains an adapter for using an `EventQueue` from [wayland-client](https://crates.io/crates/wayland-client) with an event loop that performs polling with [calloop](https://crates.io/crates/calloop).

## Requirements

Requires at least rust 1.59 to be used.
